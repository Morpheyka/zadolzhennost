/**
 * Created by morpheyka on 21.10.2017.
 *
 * @author Nikita Zelentzoff
 * @version 0.1
 */
public class Demo {

    public static ExtrasensoryFront communionThread;
    public static ExtrasensoryBack logicThread;

    public static void main(String args[]) {
        communionThread = new ExtrasensoryFront();
        logicThread = new ExtrasensoryBack();
        communionThread.start();
        logicThread.start();
    }
}
