import java.util.Scanner;


public class ExtrasensoryFront extends Thread{
    static Scanner in = new Scanner(System.in);

    public String[] agree_answers =
            {
                    //При положительном ответе
                    "-Ты станешь очень богатым!",
                    "-Скоро ты обретешь счастье!",
                    "-У тебя очень туманное будущее...я не знаю",
                    "-Ты встретишь что то необыкновенное!",
                    "-Ты получишь зачет :)",
                    "-Берегись, тебя поджидает опасноть",
                    "-Ты всё потеряешь!",
                    "-Приблизишься к войне...",
                    "-Береги близких."
            };

    String[] decline_answers = {
            //При отрицательном ответе
            "Падаете и больше не можете подняться...\nНаходите сил и выбираетесь из этого чертового места\nРаскат грома! В вас ударила молния...",
            "Спустя секунду всё наладилось и вы спокойно вышли из этого странного места.\nВ след вы услышали..\n-Берегись своего бездействия"
    };


    String[] descriptions =
            {
                    "\nВ палатке вдруг потемнело...\n" +
                            "Но ведь на улице день?!' -Подумали вы.\n" +
                            "Мгла покрыла всё, вдруг вы осознали что не видите лица старухи\n" +
                            "Как ниоткуда оно очутилось почти вплотную к вам и сказала следующее...\n",
                    "\n-Что?! Ты тратишь моё время, проваливай!\n" +
                            "Вы разгневали колдунью и поспешно пытаетесь покинуть её палатку\n" +
                            "Но у вас совсем не осталось сил...",
                    "\nВы резко поднимаетесь\n" +
                            "У вас темнеет в глаза..."
            };

    @Override
    public void run(){
        greeting();
        int numberOfAnswer = getAnswer();
        Demo.logicThread.setNumberOfAnswer(numberOfAnswer);
        actionAfterAnswer(descriptions, numberOfAnswer);
        Demo.logicThread.setSleep(false);
        while (Demo.logicThread.isAlive()){
            try {
                Demo.logicThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        outAnswer();
    }

    private void greeting(){
        System.out.println(
                "\nОбездоленный и лишившийся сил вы забрели в какую то странную палатку,\n" +
                        "увешанную черепами и прочим барахлом, стоящую среди деревьев в этих богом проклятых джунглях.\n" +
                        "-Добрый день,путник. Я старая ведьма прорицательница!\n" +
                        "-Хочешь получить ответ на все твои вопросы?\n" +
                        "Что ответить ей?\n");
        System.out.println(
                "1.Да\n" +
                        "2.Нет\n" +
                        "3.Уйти");
    }

    private int getAnswer(){
        int answer = in.nextInt();
        testInputAnswer(answer);
        return answer;
    }

    public  int testInputAnswer(int choiceAnswer) {
        while (choiceAnswer < 1 || choiceAnswer > 3) {
            System.out.println("\n-Что? Я тебя не понимаю... Скажи еще раз\n" +
                    "Что ответить ей?\n" +
                    "1.Да\n" +
                    "2.Нет\n" +
                    "3.Уйти\n");
            choiceAnswer = in.nextInt();
        }
        return choiceAnswer;
    }

    private void actionAfterAnswer(String[] descriptions, int numberOfAnswer) {
        System.out.print(descriptions[--numberOfAnswer]);
    }

    private void outAnswer(){
        System.out.println(Demo.logicThread.prediction);
    }
}
