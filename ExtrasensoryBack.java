import java.util.Random;

public class ExtrasensoryBack extends Thread {
    private boolean isSleep = true;
    private int numberOfAnswer;
    String prediction;

    Random rnd = new Random();

    public void setNumberOfAnswer(int number){
        numberOfAnswer = number;
    }
    public void setSleep(boolean sleep){
        isSleep = sleep;
    }

    @Override
    public void run() {
        while (isSleep){
            try {
                sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        prediction = takeRandomAnswer(numberOfAnswer);
    }

    public String takeRandomAnswer(int choice) {
        boolean agree;

        agree = transformAnswerForBool(choice);

        return makeAnswer(agree);
    }

    private boolean transformAnswerForBool(int answer){
        boolean result = false;
        if(answer == 1) result = true;

        return result;
    }

    private String makeAnswer(boolean agreeAnswer){
        int idAnswer;

        if(!agreeAnswer)
        {
            idAnswer = (rnd.nextInt(Demo.communionThread.decline_answers.length));
            return Demo.communionThread.decline_answers[idAnswer];
        } else {
            idAnswer = rnd.nextInt(Demo.communionThread.agree_answers.length);
            return Demo.communionThread.agree_answers[idAnswer];
        }
    }
}
